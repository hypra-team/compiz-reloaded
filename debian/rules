#!/usr/bin/make -f
# -*- makefile -*-

include /usr/share/dpkg/architecture.mk

CORE_ABIVERSION := $(shell sed -rn 's/^\#define[[:space:]]+CORE_ABIVERSION[[:space:]]+//p' include/compiz-core.h )

LDFLAGS += -Wl,--as-needed

# List of plugins passed to ./configure.  The order is important
PLUGINS=core,ccp,move,resize,place,decoration,screenshot,matecompat,png,obs,svg,clone,mousepoll,wall,glib,minimize,showmouse,neg,fade,scale,focuspoll,ezoom,switcher

CONF_FLAGS = --disable-static \
		--with-gtk=3.0 \
		--disable-fuse \
		--enable-librsvg \
		--enable-gtk \
		--enable-marco \
		--enable-menu-entries \
		--enable-xi2-events \
		--with-default-plugins=$(PLUGINS)

%:
	dh $@ --with autoreconf

override_dh_auto_configure:
	intltoolize --copy --force --automake
	dh_auto_configure -- $(CONF_FLAGS) --libdir=/usr/lib

override_dh_install:
# remove unneeded .la files
	find $(CURDIR)/debian/tmp/usr/lib \
	-type f -name '*.la' -exec rm -f {} ';'

	dh_install --fail-missing
	! mkdir debian/compiz-dev/usr/lib/$(DEB_HOST_MULTIARCH) || \
	mv debian/compiz-dev/usr/lib/pkgconfig debian/compiz-dev/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig
	! mkdir debian/libdecoration0-dev/usr/lib/$(DEB_HOST_MULTIARCH) || \
	mv debian/libdecoration0-dev/usr/lib/pkgconfig debian/libdecoration0-dev/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig

# matecompat plugin and xml file are seperately installed
# into the -mate package respectively
	rm -f debian/compiz-plugins/usr/lib/compiz/libmatecompat.so
	rm -f debian/compiz-plugins/usr/share/compiz/matecompat.xml

# core.xml excluded from plugins as it is in core
	rm -f debian/compiz-plugins/usr/share/compiz/core.xml

override_dh_makeshlibs:
	dh_makeshlibs -plibdecoration0 -V'libdecoration0 (>= 2:0.8.14-0~)'

override_dh_gencontrol:
	dh_gencontrol -- -Vcoreabiversion=$(CORE_ABIVERSION)
