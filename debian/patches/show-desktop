commit 21eae241b88fe7f68c75bb17c85c98a925ca1954
Author: Colomban Wendling <cwendling@hypra.fr>
Date:   Wed Dec 5 15:06:40 2018 +0100

    show_desktop: Show the desktop whenever any relevant window is shown
    
    This makes the "show_desktop" feature work more naturally (show the
    desktop whenever a relevant window is showing) instead of being a
    global toggle state invisible to the user.

diff --git a/src/display.c b/src/display.c
index 2c78a23d..6c222f59 100644
--- a/src/display.c
+++ b/src/display.c
@@ -279,6 +279,22 @@ maximizeVertically (CompDisplay     *d,
     return TRUE;
 }
 
+static void
+allWindowsInDesktopShowingMode (CompWindow  *w,
+				void        *closure)
+{
+    int *showingDesktop = closure;
+
+    /* see screen.c:enterShowDesktopMode() */
+    if ((w->screen->showingDesktopMask & w->wmType) &&
+	!(w->state & CompWindowStateSkipTaskbarMask) &&
+	!w->inShowDesktopMode && !w->grabbed &&
+	w->managed && (*w->screen->focusWindow) (w))
+    {
+	*showingDesktop = 0;
+    }
+}
+
 static Bool
 showDesktop (CompDisplay     *d,
 	     CompAction      *action,
@@ -294,7 +310,16 @@ showDesktop (CompDisplay     *d,
     s = findScreenAtDisplay (d, xid);
     if (s)
     {
-	if (s->showingDesktopMask == 0)
+	int isDesktopShowing = 0;
+
+	/* leave desktop mode if any relevant window is showing */
+	if (s->showingDesktopMask != 0)
+	{
+	    isDesktopShowing = 1;
+	    forEachWindowOnScreen (s, allWindowsInDesktopShowingMode, &isDesktopShowing);
+	}
+
+	if (! isDesktopShowing)
 	    (*s->enterShowDesktopMode) (s);
 	else
 	    (*s->leaveShowDesktopMode) (s, NULL);
